# README #

### What is this repository for? ###

* 通过一些配置，来生成项目中大量的基础代码
* Version: 1.0.10-SNAPSHOT
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### TODO ###



### How do I get set up? ###

* Summary of set up
> 在 project 或者 module 中的 resources 目录下创建文件夹 com/ethanxx/codemother
>
> 在目录下添加 global.properties, 内容实例如下：

```
# 默认即可，无需更改
baseOutputPath=.
outputPathJava=src/main/java
outputPathJavaResource=src/main/resources
outputWebPath=src/main/webapp
outputPagePath=src/main/webapp/WEB-INF/page/
javaOutputPath=src/main/java
resourceOutputPath=src/main/resources

# 需要根据模块的包名修改
projectId=com.primeco.log
packagePath=com/primeco/log
modelPackage=com.primeco.log.model
mapperPackage=com.primeco.log.mapper
servicePackage=com.primeco.log.service
controllerPackage=com.primeco.log.ctl
controllerBasePackage=com.primeco.log.ctl.base
```

* How to run tests
>进入项目或者模块的目录，执行命令：mvn codemother:code-generate

### Schema 文件说明 ###
    <schema tableName="ACHIEVEMENT_ALLOCATION" modelName="AchievementAllocation" paged="true" noController="true">
        <key name="id" type="integer" length="10" generator="native"></key>
    
        <column name="account_number" type="varchar" length="64"></column>
    
        <default-condition>
            <match column="user_id" param="userId"/>
            <match column="status_cd" param="statusCd"/>
            <like column="username"/>
            <match-value column="record_flag" value="1"/>
            <between column="last_login_time" javaType="java.sql.Timestamp" low="low" high="high"/>
            <in column="id"/>
        </default-condition>
    
        <default-order>
            <order-by column="username" asc="false"/>
        </default-order>
    
        <alias name="user"/>
    </schema>

* 标签 key 描述了表的主键，包含属性 name,type,length 等; 当主键为序列时，添加 generator 属性，值为 native;

* 标签 match 表示查询时精确匹配；
* 标签 like 表示查询时模糊匹配；
* 标签 match-value 表示查询时按特定的值进行查询；
* 标签 between 表示范围查询，low 和 high 分别表示查询范围的上下限；
* 标签 in 表示包含查询，其参数值一个数组，如："value1,value2,value3".split(",");
* 标签 order-by 表示查询结果按该字段进行排序；

* 属性 column 指的表示 Table 的字段名；
* 属性 javaType 表示数据库字段类型对应在 Java 中的数据类型；缺省该属性则默认为 String；
* 属性 searchType 表示该字段在 Controller 接收页面传来的值的类型；缺省该属性则默认为 String；
* 属性 param 表示该字段在 Java 代码中的命名，以驼峰形式命名；缺省该属性则默认为 column 的值；
* 属性 asc 的值决定了查询结果的排序方式，false 表示按降序排列，缺省该属性或者设置为 true 表示按升序排列结果；