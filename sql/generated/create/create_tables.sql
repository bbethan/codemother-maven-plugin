CREATE TABLE man_user (
	username VARCHAR (30),
	password VARCHAR (64),
	disp_name VARCHAR (64),
	sex VARCHAR (10),
	email VARCHAR (100),
	mobile VARCHAR (20),
	user_type VARCHAR (20),
	department VARCHAR (32),
	supervisor VARCHAR (30),
	status VARCHAR (10) DEFAULT 0001,
	last_login_time DATE,
	createuid VARCHAR (30),
	createtime DATE,
	updateuid VARCHAR (30),
	updatetime DATE,
	primary key ( username )
);

CREATE TABLE users (
	login_id INT ,
	name VARCHAR (30),
	password VARCHAR (40),
	enabled CHAR,
	createuid VARCHAR (30),
	createtime DATE,
	updateuid VARCHAR (30),
	updatetime DATE,
	primary key ( login_id )
);

CREATE TABLE man_user_role (
	username VARCHAR (30),
	role_id INT,
	primary key (		
			username,
			role_id	)
);

