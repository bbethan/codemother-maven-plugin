package com.ethanxx.codemother;

public abstract class AbstractGlobalConfig {

    protected String database;

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
}
