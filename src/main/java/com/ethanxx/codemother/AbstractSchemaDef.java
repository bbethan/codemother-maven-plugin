package com.ethanxx.codemother;

import java.util.Properties;

abstract public class AbstractSchemaDef {

    protected String name;
    
    /**
     * @date 2015年3月14日 下午4:14:16
     * @param properties
     * @Descriptoin Subclass can override this method to get global properties.
     */
    public void setGlobalProperties(Properties properties) {
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
