package com.ethanxx.codemother;

import java.util.Map;

import org.apache.commons.digester.Rule;
import org.xml.sax.Attributes;

public class AddMapEntryRule extends Rule {

    private String key;
    private String keyName;

    public AddMapEntryRule() {
        super();
    }

    public AddMapEntryRule(String keyname) {
        super();
        this.keyName = keyname;
    }

    @Override
    public void begin(String namespace, String name, Attributes attributes) throws Exception {
        super.begin(namespace, name, attributes);

        if (attributes.getLength() < 1) {
            throw new RuntimeException("Cannot choose attribute for key");
        }
        if (keyName != null) {
            key = attributes.getValue(keyName);
        } else if (attributes.getValue("key") != null) {
            key = attributes.getValue("key");
        } else if (attributes.getValue("name") != null) {
            key = attributes.getValue("name");
        } else {
            key = attributes.getValue(0);
        }
        if (key == null) {
            throw new RuntimeException("Key value is null");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void end(String namespace, String name) throws Exception {
        if (key != null) {
            Object current = digester.peek(0);
            Object parent = digester.peek(1);
            if (!(parent instanceof Map<?, ?>)) {
                throw new RuntimeException("Parent should be an instance of Map");
            }
            ((Map<String, Object>) parent).put(key, current);
        }
        key = null;
        super.end(namespace, name);
    }
}
