package com.ethanxx.codemother;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

abstract public class BaseMojo extends AbstractMojo {
    /**
     * @parameter property="project"
     */
    protected MavenProject project;
    /**
     * @parameter property="globalConfigParserClassName" default-value="com.ethanxx.codemother.model.GlobalConfigParser"
     */
    protected String globalConfigParserClassName;
    /**
     * @parameter property="schemaParserClassName" default-value="com.ethanxx.codemother.model.SchemaParser"
     */
    protected String schemaParserClassName;
    /**
     * @parameter property="codemotherPath" default-value="/com/ethanxx/codemother"
     */
    protected String codemotherPath;
    /**
     * @parameter property="templatePath" default-value="../codemother/template"
     */
    protected String templatePath;
    /**
     * @parameter property="configPath" default-value="/com/ethanxx/codemother/dbschema/config"
     */
    protected String configPath;
    /**
     * @parameter property="golbalConfigPath" default-value="src/main/resources/com/ethanxx/codemother"
     */
    protected String golbalConfigPath;
    /**
     * @parameter property="schemaPath" default-value="src/main/resources/com/ethanxx/codemother/dbschema"
     */
    protected String schemaPath;

    protected GlobalConfigParserIntf globalConfigParser;
    protected SchemaParserIntf schemaParser;
    protected MavenPathUtils mavenPathUtils;
    protected List<TemplateDesc> singleTemplates;
    protected List<TemplateDesc> globalTemplates;
    protected List<AbstractSchemaDef> schemaList;
    protected Properties globalProperties = new Properties();
    protected Map<String, AbstractSchemaDef> schemaMap = new HashMap<String, AbstractSchemaDef>();
    protected AbstractGlobalConfig globalConfig;

    @SuppressWarnings("unchecked")
    protected void init() throws MojoExecutionException {
        mavenPathUtils = new MavenPathUtils(project);
        try {
            globalProperties.load(new FileInputStream(new File(mavenPathUtils.getFile(golbalConfigPath), "global.properties")));
            // globalProperties.load(this.getClass().getResourceAsStream(codemotherPath + "global.properties"));
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to load global properties", e);
        }

        try {
            globalConfigParser = ((Class<GlobalConfigParserIntf>) getClass().getClassLoader()
                    .loadClass(globalConfigParserClassName)).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new MojoExecutionException("Failed to instanit global config parser");
        }

        try {
            schemaParser = ((Class<SchemaParserIntf>) getClass().getClassLoader().loadClass(
                    schemaParserClassName)).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new MojoExecutionException("Failed to instanit schema parser");
        }

    }

    protected void readTemplates() throws MojoExecutionException {
        this.singleTemplates = new ArrayList<>();
        this.globalTemplates = new ArrayList<>();
        /*ClassLoader classLoader = this.getClass().getClassLoader();
        URL url = classLoader.getResource(templatePath);
        if (url != null) {
            walkInPath(singleTemplates, new File(mavenPathUtils.getFile(url.getPath()), "single"), "single");
            walkInPath(globalTemplates, new File(mavenPathUtils.getFile(url.getPath()), "context"), "context");
        }*/
        walkInPath(singleTemplates, new File(mavenPathUtils.getFile(templatePath), "single"), "single");
        walkInPath(globalTemplates, new File(mavenPathUtils.getFile(templatePath), "context"), "context");
    }

    protected void walkInPath(List<TemplateDesc> templateList, File dir, String path)
            throws MojoExecutionException {
        File[] files = dir.listFiles();
        if (files == null || files.length == 0)
            return;
        for (File file : files) {
            if (file.isDirectory()) {
                walkInPath(templateList, file, path + "/" + file.getName());
            } else if (file.isFile()
                    && StringUtils.equals("ftl", FilenameUtils.getExtension(file.getName()))
                    && !FilenameUtils.getBaseName(file.getName()).endsWith("-inc")) {
                // read and store template
                TemplateDesc td = new TemplateDesc();
                td.setPath(path);
                String filename = FilenameUtils.removeExtension(file.getName());
                td.setTemplate(path + "/" + file.getName());
                File propertyFile = new File(dir, filename + ".properties");
                if (propertyFile.exists()) {
                    Properties props = new Properties();
                    try {
                        props.load(new FileInputStream(propertyFile));
                        td.setProperties(props);
                    } catch (IOException e) {
                        getLog().error(e);
                        throw new MojoExecutionException("Failed to read properties" + propertyFile.getName());
                    }
                } else {
                    throw new MojoExecutionException(propertyFile.getName() + " is required");
                }
                templateList.add(td);
            }
        }
    }

    /**
     * 2015年3月16日 下午7:36:01
     *
     * @throws MojoExecutionException
     */
    protected void readAndParseModel() throws MojoExecutionException {
        File schemaDir = mavenPathUtils.getFile(schemaPath);
        if (!schemaDir.exists() || !schemaDir.isDirectory()) {
            throw new MojoExecutionException("Cannot find schema directory");
        }
        // this.parseGlobal(mavenPathUtils.getFile(configPath));
        this.parseGlobal();
        File[] schemaFileArr = schemaDir.listFiles();
        schemaList = new ArrayList<>();
        assert schemaFileArr != null;
        for (File schemaFile : schemaFileArr) {
            if ("global.xml".equals(schemaFile.getName()) || !StringUtils.equals("xml", FilenameUtils.getExtension(schemaFile.getName()))) {
                // Skip global.xml
                continue;
            }
            getLog().debug("schemaFile: " + schemaFile.getName());
            InputStream in = null;
            try {
                in = new FileInputStream(schemaFile);
                AbstractSchemaDef schemaDef = schemaParser.parse(in);
                if (schemaDef == null) {
                    String content = "Failed to parse schema file :" + schemaFile.getName();
                    getLog().error(content);
                    throw new MojoExecutionException(content);
                }
                schemaDef.setGlobalProperties(globalProperties);
                schemaList.add(schemaDef);
            } catch (IOException e) {
                String content = "Failed to read schema file :" + schemaFile.getName();
                getLog().error(content);
                throw new MojoExecutionException(content, e);
            } catch (SAXException e) {
                String content = "Failed to parse schema file :" + schemaFile.getName();
                getLog().error(content);
                throw new MojoExecutionException(content, e);
            } finally {
                IOUtils.closeQuietly(in);
            }
        }
        for (AbstractSchemaDef def : schemaList) {
            schemaMap.put(def.getName(), def);
        }
//        }
    }

    /**
     * @param configDir
     * @throws MojoExecutionException
     * @deprecated
     */
    private void parseGlobal(File configDir) throws MojoExecutionException {
        File globalConfigFile = new File(configDir, "global.xml");
        if (!globalConfigFile.exists()) {
            throw new MojoExecutionException("Failed to find global config file");
        }
        InputStream input = null;
        try {
            input = new FileInputStream(globalConfigFile);
            this.globalConfig = this.globalConfigParser.parse(input);
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to read global config file");
        } catch (SAXException e) {
            throw new MojoExecutionException("Failed to parse global config file");
        } finally {
            IOUtils.closeQuietly(input);
        }
        schemaParser.init(globalConfig);
    }

    private void parseGlobal() throws MojoExecutionException {
        InputStream input = null;
        try {
            input = this.getClass().getResourceAsStream(configPath + "/global.xml");
            this.globalConfig = this.globalConfigParser.parse(input);
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to read global config file");
        } catch (SAXException e) {
            throw new MojoExecutionException("Failed to parse global config file");
        } finally {
            IOUtils.closeQuietly(input);
        }
        schemaParser.init(globalConfig);
    }

    protected Map<String, Object> createParameters() {
        Map<String, Object> params = new HashMap<>();
        params.put("config", this.globalConfig);
        for (Map.Entry<Object, Object> entry : globalProperties.entrySet()) {
            params.put((String) entry.getKey(), entry.getValue());
        }
        params.put("schemaMap", schemaMap);
        return params;
    }

    public MavenProject getProject() {
        return project;
    }

    public void setProject(MavenProject project) {
        this.project = project;
    }

    public String getCodemotherPath() {
        return codemotherPath;
    }

    public void setCodemotherPath(String codemotherPath) {
        this.codemotherPath = codemotherPath;
    }

    public String getSchemaPath() {
        return schemaPath;
    }

    public void setSchemaPath(String schemaPath) {
        this.schemaPath = schemaPath;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getSchemaParserClassName() {
        return schemaParserClassName;
    }

    public void setSchemaParserClassName(String schemaParserClassName) {
        this.schemaParserClassName = schemaParserClassName;
    }

    public String getGlobalConfigParserClassName() {
        return globalConfigParserClassName;
    }

    public void setGlobalConfigParserClassName(String globalConfigParserClassName) {
        this.globalConfigParserClassName = globalConfigParserClassName;
    }

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }

    public String getGolbalConfigPath() {
        return golbalConfigPath;
    }

    public void setGolbalConfigPath(String golbalConfigPath) {
        this.golbalConfigPath = golbalConfigPath;
    }
}
