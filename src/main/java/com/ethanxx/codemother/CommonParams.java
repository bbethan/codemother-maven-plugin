package com.ethanxx.codemother;

public class CommonParams {
    public static final String NAME_PATTERN = "name.pattern";
    public static final String OUTPUT_PATH = "output.path";
    public static final String OUTPUT_PACKAGE = "output.package";
    public static final String OVERWRITE = "overwrite";
    public static final String IGNORE_THIS = "ignoreThis";
}
