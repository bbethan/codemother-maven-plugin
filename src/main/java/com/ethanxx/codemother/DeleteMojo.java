package com.ethanxx.codemother;

import java.io.File;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.mvel2.templates.TemplateRuntime;

/**
 * @Date 2015年3月13日
 * @goal delete-generated
 */
public class DeleteMojo extends BaseMojo {

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        // System.out.println(project.getName());
        init();
        readTemplates();
        readAndParseModel();
        deleteContext();
        deleteSingle();
    }

    private void deleteSingle() {
        for (AbstractSchemaDef def : this.schemaList) {
            for (TemplateDesc td : singleTemplates) {
                Map<String, Object> params = createParameters();
                params.put("schema", def);
                // fill properties
                td.fillProperties(params);

                String outputName = (String) TemplateRuntime.eval(td.getNamePattern(), params);
                String outputPath = td.getOutputPath(params);
                File outputDir = mavenPathUtils.getFile(outputPath);
                if (outputDir.exists()) {
                    File outputFile = new File(outputDir, outputName);
                    if(outputFile.exists()) {
                        if (td.isOverwrite()) {
                            if (!outputFile.delete()) {
                                getLog().debug("failed to delete file:" + outputFile.getPath());
                            }
                        }
                    }
                }
            }
        }
    }

    private void deleteContext() {
        for (TemplateDesc td : globalTemplates) {
            Map<String, Object> params = createParameters();
            params.put("schemaList", schemaList);
            td.fillProperties(params);
            String outputFileName = (String) TemplateRuntime.eval(td.getNamePattern(), params);
            String outputPath = td.getOutputPath(params);
            File outputDir = mavenPathUtils.getFile(outputPath);
            if (!outputDir.exists()) {
                continue;
            }
            File outputFile = new File(outputDir, outputFileName);
            if (outputFile.exists()) {
                continue;
            }
            if (!outputFile.delete()) {
                getLog().debug("failed to delete file:" + outputFile.getPath());
            }
        }
    }
}
