package com.ethanxx.codemother;

import org.apache.commons.digester.Digester;

public class DigesterUtil {

    public static void addChildRule(Digester dig, String pattern, Class<?> childClass, String addNextMethod) {
        createObjWithProps(dig, pattern, childClass);
        dig.addSetNext(pattern, addNextMethod);
    }

    public static void addMapChild(Digester dig, String childPattern, Class<?> childClass, String mapKey) {
        dig.addObjectCreate(childPattern, childClass);
        dig.addSetProperties(childPattern);
        dig.addRule(childPattern, new AddMapEntryRule(mapKey));
    }

    public static void createObjWithProps(Digester dig, String pattern, Class<?> clazz) {
        dig.addObjectCreate(pattern, clazz);
        dig.addSetProperties(pattern);
    }
}
