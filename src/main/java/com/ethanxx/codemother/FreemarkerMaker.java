package com.ethanxx.codemother;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.MojoExecutionException;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerMaker {
    private Configuration config;

    FreemarkerMaker(File templatePath) throws IOException {
        config = new Configuration(Configuration.VERSION_2_3_22);
        setTemplatePath(templatePath);
    }

    public void render(Map<String, Object> data, String templateName, File outputFile)
            throws MojoExecutionException {
        String charset = (String) data.get("charset");
        Template tmpl;

        try {
            if (charset != null) {
                tmpl = config.getTemplate(templateName, charset);
            } else {
                tmpl = config.getTemplate(templateName);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new MojoExecutionException("Failed to load template:" + templateName);
        }
        Writer w = null;
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(outputFile);

            if (charset != null) {
                w = new OutputStreamWriter(os, charset);
            } else {
                w = new OutputStreamWriter(os);
            }
            tmpl.process(data, w);
            w.flush();
        } catch (TemplateException e) {
            throw new MojoExecutionException("Failed to process template:" + templateName);
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to render file " + outputFile.getName());
        } finally {
            if (w != null) {
                try {
                    w.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            IOUtils.closeQuietly(os);
        }
    }

    private void setTemplatePath(File templatePath) throws IOException {
        config.setDirectoryForTemplateLoading(templatePath);
    }
}
