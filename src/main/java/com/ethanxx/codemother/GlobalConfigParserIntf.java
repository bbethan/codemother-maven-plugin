package com.ethanxx.codemother;

import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.SAXException;

public interface GlobalConfigParserIntf {
    AbstractGlobalConfig parse(InputStream in) throws IOException, SAXException;
}
