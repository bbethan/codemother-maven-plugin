package com.ethanxx.codemother;

import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;

public class MavenPathUtils {

    private MavenProject project;

    public MavenPathUtils(MavenProject project) {
        this.project = project;
    }

    public File getFile(String path) {
        File target = new File(path);
        if (!target.isAbsolute()) {
            target = new File(project.getBasedir(), path);
        }
        return target;
    }

    public Path getPath(String dirPath) {
        try {
            ClassLoader classLoader = this.getClass().getClassLoader();
            URL url = classLoader.getResource(dirPath);
            if (url != null) {
                URI uri = url.toURI();
                Path path;
                if (uri.getScheme().equals("jar")) {
                    FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
                    path = fileSystem.getPath(dirPath);
                } else {
                    path = Paths.get(uri);
                }
                Path walk = Files.walkFileTree(path, new FileVisitor() {
                    @Override
                    public FileVisitResult preVisitDirectory(Object dir,
                                                             BasicFileAttributes attrs) throws IOException {
                        //访问文件夹之前调用
                        System.out.println(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Object file,
                                                     BasicFileAttributes attrs) throws IOException {
                        // 访问文件调用
                        System.out.println(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Object file, IOException exc)
                            throws IOException {
                        // 访问文件失败时调用
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Object dir,
                                                              IOException exc) throws IOException {
                        // 访问文件夹之后调用
                        return FileVisitResult.CONTINUE;
                    }

                });
                return walk;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        /*try {
            CodeSource src = this.getClass().getProtectionDomain().getCodeSource();
            List<String> list = new ArrayList<String>();

            if (src != null) {
                URL jar = src.getLocation();
                ZipInputStream zip = new ZipInputStream(jar.openStream());
                ZipEntry ze = null;

                while ((ze = zip.getNextEntry()) != null) {
                    String entryName = ze.getName();
                    list.add(entryName);
                    System.out.println(entryName);
                }

            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;*/
    }

}
