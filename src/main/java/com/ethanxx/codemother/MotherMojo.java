package com.ethanxx.codemother;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.codehaus.plexus.util.StringUtils;
import org.mvel2.templates.TemplateRuntime;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Date 2015年3月12日
 * @goal code-generate
 */
public class MotherMojo extends BaseMojo {

    /**
     * @parameter property="outputLog" default-value="output_file.lst"
     */
    private String outputLog;

    private List<File> outputtedFiles = new ArrayList<File>();
    protected FreemarkerMaker freemarkerMaker;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        init();
        readTemplates();
        readAndParseModel();
        renderSingle();
        renderContext();
        saveOutputtedFiles();
    }

    private void renderContext() throws MojoExecutionException {
        for (TemplateDesc td : globalTemplates) {
            Map<String, Object> params = createParameters();
            params.put("schemaList", schemaList);
            td.fillProperties(params);
            String outputFileName = (String) TemplateRuntime.eval(td.getNamePattern(), params);
            String outputPath = td.getOutputPath(params);
            File outputDir = mavenPathUtils.getFile(outputPath);
            if (!outputDir.exists()) {
                if (outputDir.mkdirs()) {
                    outputtedFiles.add(outputDir);
                }
            }
            File outputFile = new File(outputDir, outputFileName);
            outputtedFiles.add(outputFile);
            freemarkerMaker.render(params, td.getTemplate(), outputFile);
        }

    }

    private void saveOutputtedFiles() {
        File outputFile = new File(outputLog);
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(outputFile, "UTF-8");
            for (File outFile : outputtedFiles) {
                pw.println(outFile.getPath());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(pw);
        }

    }

    private void renderSingle() throws MojoExecutionException {
        for (AbstractSchemaDef def : this.schemaList) {
            for (TemplateDesc td : singleTemplates) {
                Map<String, Object> params = createParameters();
                params.put("schema", def);
                // fill properties
                td.fillProperties(params);
                String condition = (String) params.get("generate.condition");
                if (condition != null && "false".equalsIgnoreCase(condition)) {
                    continue;
                }

                if (td.hasInclude()) {
                    File includeFile = new File(mavenPathUtils.getFile(td.getIncludePath()), td.getIncludeName(params));
                    String includeToken = td.getIncludeToken();
                    if (includeFile.exists()) {
                        try {
                            String includeContent = StringUtils.trim(FileUtils
                                    .readFileToString(includeFile));
                            if (includeContent.startsWith("<root>")) {
                                includeContent = includeContent.substring(7,
                                        includeContent.length() - 7);
                            }
                            params.put(includeToken, includeContent);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                String outputName = (String) TemplateRuntime.eval(td.getNamePattern(), params);
                String outputPath = td.getOutputPath(params);
                File outputDir = mavenPathUtils.getFile(outputPath);
                if (!outputDir.exists()) {
                    if (!outputDir.mkdirs()) {
                        throw new MojoExecutionException("Failed to make direcotry " + outputDir.getName());
                    }
                    outputtedFiles.add(outputDir);
                }

                File outputFile = new File(outputDir, outputName);
                if (!td.isOverwrite() && outputFile.exists()) {
                    continue;
                }

                outputtedFiles.add(outputFile);
                freemarkerMaker.render(params, td.getTemplate(), outputFile);
            }
        }
    }

    /**
     * 初始化 FreeMarkerMaker 的模板路径
     */
    @Override
    protected void init() throws MojoExecutionException {
        super.init();
        try {
            /*ClassLoader classLoader = this.getClass().getClassLoader();
            URL url = classLoader.getResource(templatePath);
            if (url != null) {
                freemarkerMaker = new FreemarkerMaker(mavenPathUtils.getFile(url.getPath()));
            }*/
            freemarkerMaker = new FreemarkerMaker(mavenPathUtils.getFile(templatePath));
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to set template path");
        }
    }

    public String getOutputLog() {
        return outputLog;
    }

    public void setOutputLog(String outputLog) {
        this.outputLog = outputLog;
    }

}
