package com.ethanxx.codemother;

import com.ethanxx.codemother.model.SchemaDef;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

public interface SchemaParserIntf {
	void init(AbstractGlobalConfig globalConfig);
	SchemaDef parse(InputStream in) throws IOException, SAXException;
}
