package com.ethanxx.codemother;

import com.ethanxx.codemother.freemaker.LowUnderMethod;
import com.ethanxx.codemother.freemaker.Native2AsciiMethod;
import com.ethanxx.codemother.utils.ConfigConstants;
import com.ethanxx.codemother.utils.LocalUtil;
import org.codehaus.plexus.util.StringUtils;
import org.mvel2.templates.TemplateRuntime;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class TemplateDesc {
    private Native2AsciiMethod native2AsciiMethod;
    private LowUnderMethod lowUnderMethod;
    private String path;
    private String template;
    private Properties properties;

    public TemplateDesc() {
        this.native2AsciiMethod = new Native2AsciiMethod();
        this.lowUnderMethod = new LowUnderMethod();
    }

    public String getNamePattern() {
        if (properties != null) {
            return properties.getProperty(CommonParams.NAME_PATTERN);
        }
        return null;
    }

    public boolean hasInclude() {
        if (properties.containsKey("includePath") && properties.containsKey("includeToken")
                && properties.containsKey("include.name.pattern")) {
            return true;
        }
        return false;
    }

    public String getIncludePath() {
        return properties.getProperty("includePath");
    }

    public String getIncludeName(Map<String, Object> params) {
        return (String) TemplateRuntime.eval(properties.getProperty("include.name.pattern"), params);
    }

    public String getIncludeToken() {
        return properties.getProperty("includeToken");
    }

    public String getOutputPath(Map<String, Object> params) {
        String outputPath = getProperty(CommonParams.OUTPUT_PATH, params);
        String packageProperty = properties.getProperty(CommonParams.OUTPUT_PACKAGE);
        String outputPackage = packageProperty == null ? null :
                (String) TemplateRuntime.eval(packageProperty, params);
        if (!StringUtils.isEmpty(outputPackage)) {
            String replace = outputPackage.replace('.', File.separatorChar);
            outputPath = outputPath + File.separator + replace;
        }
        return outputPath;
    }

    protected String getProperty(String name, Map<String, Object> params) {
        return (String) TemplateRuntime.eval(properties.getProperty(name), params);
    }

    public void fillProperties(Map<String, Object> params) {
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            String value = (String) entry.getValue();
            Object result = TemplateRuntime.eval(value, params);
            params.put((String) entry.getKey(), result == null ? "" : result.toString());
        }
    }

    public boolean isOverwrite() {
        String overwriteVal = properties.getProperty(CommonParams.OVERWRITE);
        if (!StringUtils.isEmpty(overwriteVal)) {
            return Boolean.valueOf(overwriteVal);
        }
        return true;
    }

    public boolean isIgnore(AbstractSchemaDef def, TemplateDesc td) {
        Properties prop = new Properties();

        try {
            prop.load(new FileInputStream(new File(ConfigConstants.GLOBAL_PROPERTIES)));
            Map rootContext = this.createContext((Map)null, prop);
            rootContext.put("entity", def);
            Map localContext = this.createContext(rootContext, td.getProperties());
            String ignoreThis = LocalUtil.extractStringValue(localContext, CommonParams.IGNORE_THIS);
            if(!StringUtils.isEmpty(ignoreThis)) {
                return Boolean.valueOf(ignoreThis).booleanValue();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

    private Map<String, Object> createContext(Map<String, Object> upperContext, Properties config) {
        HashMap resultContext = new HashMap();
        resultContext.put("n2a", this.native2AsciiMethod);
        resultContext.put("l_u", this.lowUnderMethod);
        if(upperContext != null) {
            resultContext.putAll(upperContext);
        }

        for(Map.Entry entry : config.entrySet()) {
            String rawValue = entry.getValue().toString();

            try {
                Object value = TemplateRuntime.eval(rawValue, resultContext);
                resultContext.put(entry.getKey().toString(), value);
            } catch (Exception e) {
                System.out.println("Failed to process value " + rawValue + " with " + e.getMessage());
            }
        }

        return resultContext;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

}
