package com.ethanxx.codemother.freemaker;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.util.List;

/**
 * Created by ethan on 16/02/2017.
 */
public class LowUnderMethod implements TemplateMethodModelEx {
    public LowUnderMethod() {
    }

    public Object exec(List list) throws TemplateModelException {
        if (list != null && list.size() == 1) {
            Object a = list.get(0);
            if (!(a instanceof String)) {
                return null;
            } else {
                String str = (String) a;
                StringBuilder strb = new StringBuilder();
                StringBuilder part = new StringBuilder();

                for (int i = 0; i < str.length(); ++i) {
                    char ch = str.charAt(i);
                    if (65 <= ch && 90 >= ch && part.length() > 0) {
                        if (strb.length() > 0) {
                            strb.append('_');
                        }

                        strb.append(part);
                        part.delete(0, part.length());
                    }

                    part.append(Character.toLowerCase(ch));
                }

                if (part.length() > 0) {
                    if (strb.length() > 0) {
                        strb.append('_');
                    }

                    strb.append(part);
                }

                return strb.toString();
            }
        } else {
            throw new TemplateModelException("Wrong argument");
        }
    }
}
