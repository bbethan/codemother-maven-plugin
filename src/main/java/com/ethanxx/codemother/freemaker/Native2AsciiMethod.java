package com.ethanxx.codemother.freemaker;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.util.List;

/**
 * Created by ethan on 16/02/2017.
 */
public class Native2AsciiMethod implements TemplateMethodModelEx {
    public Native2AsciiMethod() {
    }

    public Object exec(List list) throws TemplateModelException {
        if (list != null && list.size() == 1) {
            Object a = list.get(0);
            StringBuilder strb = new StringBuilder();
            if (a instanceof String) {
                String str = (String) a;

                for (int i = 0; i < str.length(); ++i) {
                    char chr = str.charAt(i);
                    if (chr > 255) {
                        strb.append("\\u");
                        strb.append(Integer.toHexString(chr));
                    } else {
                        strb.append(chr);
                    }
                }
            }

            return strb.toString();
        } else {
            throw new TemplateModelException("Wrong argument");
        }
    }
}
