package com.ethanxx.codemother.generator;

import java.util.Map;

import com.ethanxx.codemother.utils.CmDBUtils;
import com.ethanxx.codemother.utils.FileUtils;
import com.ethanxx.codemother.utils.PropertiesUtils;
import com.ethanxx.codemother.utils.CmStringUtils;

public class BaseGenerator {

    public static String generate(String template, String tableName) {
        String content = FileUtils.getTemplate(template);

        Map<String, String> pMap = CmDBUtils.getPrimaryKeys(tableName);
        String replacedContent = null;
        String ClassName = CmStringUtils.firstUpperAndNoPrefix(tableName);
        String className = CmStringUtils.formatAndNoPrefix(tableName);
        String packageName = PropertiesUtils.getPackage();
        String primaryKeyType = pMap.get("primaryKeyType");
        String primaryKey = CmStringUtils.format(pMap.get("primaryKey"));
        String PrimaryKey = CmStringUtils.firstUpperNoFormat(primaryKey);
        if (primaryKey != null) {
            replacedContent = content.replaceAll("[$][{]ClassName}", ClassName)
                    .replaceAll("[$][{]className}", className)
                    .replaceAll("[$][{]packageName}", packageName)
                    .replaceAll("[$][{]primaryKeyType}", primaryKeyType)
                    .replaceAll("[$][{]}", primaryKeyType)
                    .replaceAll("[$][{]primaryKey}", primaryKey)
                    .replaceAll("[$][{]PrimaryKey}", PrimaryKey);
        }

        return replacedContent;
    }
}
