package com.ethanxx.codemother.generator;

import com.ethanxx.codemother.MotherMojo;
import com.ethanxx.codemother.utils.*;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class SchemaGenerator extends MotherMojo {
    private List<String> tableList;
    private String columnName = null;
    private String dataTypeName = null;
    private int columnSize = 0;
    private String columnDef = null;
    private String remarks = null;

    public void generatorSchemas() {
        FileUtils.createSchemaDirectory();

        this.setTableList();

        // generate the schema file of every table
        for (String tableName : this.tableList) {
            Map<String, String> pkMap = null;
            try {
                pkMap = CmDBUtils.getPrimaryKeys(tableName);
            } catch (Exception e) {
                System.err.println(tableName + " doesn't exist or connection exception, please check it.");
                return;
            }

            if (!pkMap.isEmpty()) {
                String layers = PropertiesUtils.getLayers();
                if (layers.contains("dbschema")) {
                    this.generateSchema(tableName, pkMap);
                }
                System.out.println(tableName + " has been generated.");
            } else {
                System.err.println(tableName + " has no pk, ignored.");
            }
        }

        System.out.println("All finished.");
    }

    /**
     * Generate the schema file by table name and the primary key map.
     *
     * @param tableName
     * @param pkMap
     */
    private void generateSchema(String tableName, Map<String, String> pkMap) {
        DatabaseMetaData meta = CmDBUtils.getDatabaseMetaData();
        String modelName = CmStringUtils.firstUpperAndNoPrefix(tableName);

        try {
            StringBuilder schemaSb = new StringBuilder();
            schemaSb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<schema ");
            schemaSb.append("name=\"" + tableName.toLowerCase());
            schemaSb.append("\" schema=\"" + PropertiesUtils.getSchema());
            schemaSb.append("\" tableName=\"" + tableName);
            schemaSb.append("\" modelName=\"" + modelName);
            schemaSb.append("\" paged=\"false\" noController=\"true\">\n");

            StringBuilder sbKeys = new StringBuilder();
            StringBuilder sbColumns = new StringBuilder();

            ResultSet columns = meta.getColumns(null, PropertiesUtils.getSchema(), tableName, "%");
            if (pkMap.size() > 1) {
                sbKeys.append("    <complex-key>\n");
                while (columns.next()) {
                    setColumnProperties(columns);
                    if (pkMap.containsKey(this.columnName.toLowerCase())) {
                        sbKeys.append("    ").append(appendColumnProperties());
                    } else {
                        sbColumns.append(appendColumnProperties());
                    }
                }
                sbKeys.append("    </complex-key>\n");
            } else {
                while (columns.next()) {
                    setColumnProperties(columns);
                    if (pkMap.containsKey(this.columnName.toLowerCase())) {
                        sbKeys.append("    <key name=\"" + this.columnName + "\" type=\"" + this.dataTypeName.toLowerCase() + "\" length=\"" + this.columnSize);
                        if (CmStringUtils.isNotEmpty(this.remarks)) {
                            sbKeys.append("\" comment=\"" + this.remarks);
                        }
                        sbKeys.append("\"/>\n");
                    } else {
                        sbColumns.append(appendColumnProperties());
                    }
                }
            } // else end

            schemaSb.append(sbKeys);
            schemaSb.append("\n");
            schemaSb.append(sbColumns);

            schemaSb.append("\n    <default-condition>\n");
            schemaSb.append("    </default-condition>\n");

            schemaSb.append("\n    <default-order>\n");
            schemaSb.append("    </default-order>\n");

            schemaSb.append("\n    <alias name=\"" + modelName + "\"/>\n");

            schemaSb.append("</schema>\n");

            String dbSchemaDir = ConfigConstants.SCHEMA_PATH;
            FileUtils.write(schemaSb.toString(), dbSchemaDir + CmStringUtils.firstUpperAndNoPrefix(tableName) + ".xml");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the list of table's name
     * Created on 8/18.
     */
    private void setTableList() {
        try {
            this.tableList = PropertiesUtils.getTableList();
            if (this.tableList.size() == 0) {
                this.tableList = CmDBUtils.getAllTables();
            }
        } catch (Exception e) {
            System.err.println("connection exception, please check it.");
            return;
        }
    }

    private void setColumnProperties(ResultSet columns) {
        try {
            this.columnName = columns.getString("COLUMN_NAME");// 列名
            // 对应的java.sql.Types类型
            // int dataType = columns.getInt("DATA_TYPE");
            // java.sql.Types 类型名称
            this.dataTypeName = columns.getString("TYPE_NAME");
            this.columnSize = columns.getInt("COLUMN_SIZE"); // 列大小
            // 小数位数
            // int decimalDigits = columns.getInt("DECIMAL_DIGITS");
            // 基数（通常是10或2）
            // int numPrecRadix = columns.getInt("NUM_PREC_RADIX");
            // int nullAble = columns.getInt("NULLABLE"); // 是否允许为null
            this.remarks = columns.getString("REMARKS"); // 列描述
            this.columnDef = columns.getString("COLUMN_DEF"); // 默认值
            // sql 数据类型
            // int sqlDataType = columns.getInt("SQL_DATA_TYPE");
            // int sqlDatetimeSub = columns.getInt("SQL_DATETIME_SUB"); //
            // for char types the maximum number of bytes in the column
            // int charOctetLength = columns.getInt("CHAR_OCTET_LENGTH");
            // java.sql.Types 类型名称
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private StringBuffer appendColumnProperties() {
        StringBuffer column = new StringBuffer();
        column.append("    <column name=\"" + this.columnName + "\" type=\"" + this.dataTypeName.toLowerCase() + "\" length=\"" + this.columnSize);
        if (CmStringUtils.isNotEmpty(this.columnDef)) {
            column.append("\" default=\"" + this.columnDef);
        }
        if (CmStringUtils.isNotEmpty(this.remarks)) {
            column.append("\" comment=\"" + this.remarks);
        }
        column.append("\"/>\n");
        return column;
    }
}
