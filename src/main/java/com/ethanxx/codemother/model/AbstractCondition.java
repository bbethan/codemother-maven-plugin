package com.ethanxx.codemother.model;

import org.apache.commons.lang3.StringUtils;

abstract public class AbstractCondition {
    protected String column;
    protected String param;
    protected String javaType;
    protected String searchType;

    abstract public String getType();

    public String getParam() {
        if (param == null) {
            return column;
        }
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getJavaType() {
        if(StringUtils.isEmpty(javaType)) {
            return "String";
        }
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getSearchType() {
        if(StringUtils.isEmpty(searchType)) {
            return "String";
        }
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }
}
