package com.ethanxx.codemother.model;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRelated {

    private String schemaName;
    private String alias;
    private String property;
    private List<AbstractRelated> relatedList = new ArrayList<AbstractRelated>();

    public void addRelated(AbstractRelated related) {
        relatedList.add(related);
    }

    abstract public String getTag();

    public List<AbstractRelated> getRelatedList() {
        return relatedList;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
