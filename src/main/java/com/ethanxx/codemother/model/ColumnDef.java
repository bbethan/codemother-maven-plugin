package com.ethanxx.codemother.model;

import org.apache.commons.lang3.StringUtils;

public class ColumnDef {

    private String name;
    private String javaName;
    private String dispName;
    private String type;
    private int length;
    private String comment;

    private String searchType;
    private boolean sortable;
    private boolean noUpdate;
    private boolean noInsert;
    private String insertValue;
    private String updateValue;
    private String defaultValue;

    private boolean editable;
    private boolean showInGrid;
    private String formType;
    private String formOptionRef;

    public String getJavaName() {
        if (javaName == null) {
            javaName = makeJavaName();
        }
        return javaName;
    }

    private String makeJavaName() {
        String[] cols = name.split("_");
        StringBuilder sb = new StringBuilder();
        boolean begin = true;
        for (String col : cols) {
            if (begin) {
                begin = false;
                sb.append(StringUtils.uncapitalize(col.toLowerCase()));
            } else {
                sb.append(StringUtils.capitalize(col.toLowerCase()));
            }
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJavaName(String javaName) {
        this.javaName = javaName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isNoUpdate() {
        return noUpdate;
    }

    public void setNoUpdate(boolean noUpdate) {
        this.noUpdate = noUpdate;
    }

    public boolean isNoInsert() {
        return noInsert;
    }

    public void setNoInsert(boolean noInsert) {
        this.noInsert = noInsert;
    }

    public String getInsertValue() {
        return insertValue;
    }

    public void setInsertValue(String insertValue) {
        this.insertValue = insertValue;
    }

    public String getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(String updateValue) {
        this.updateValue = updateValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDispName() {
        return dispName;
    }

    public void setDispName(String dispName) {
        this.dispName = dispName;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormOptionRef() {
        return formOptionRef;
    }

    public void setFormOptionRef(String formOptionRef) {
        this.formOptionRef = formOptionRef;
    }

    public boolean isShowInGrid() {
        return showInGrid;
    }

    public void setShowInGrid(boolean showInGrid) {
        this.showInGrid = showInGrid;
    }

    public String getComment() {
        if (comment == null) {
            comment = name;
        }
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
