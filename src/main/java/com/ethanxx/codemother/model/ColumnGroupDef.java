package com.ethanxx.codemother.model;

import java.util.ArrayList;
import java.util.List;

public class ColumnGroupDef {
    private String name;
    protected List<ColumnDef> columnDefList = new ArrayList<ColumnDef>();

    public ColumnGroupDef() {
    }

    public void addColumn(ColumnDef def) {
        columnDefList.add(def);
    }

    public List<ColumnDef> getColumnDefList() {
        return columnDefList;
    }

    public void setColumnDefList(List<ColumnDef> columnDefList) {
        this.columnDefList = columnDefList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
