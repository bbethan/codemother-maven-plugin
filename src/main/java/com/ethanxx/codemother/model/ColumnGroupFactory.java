package com.ethanxx.codemother.model;

import org.apache.commons.digester.AbstractObjectCreationFactory;
import org.xml.sax.Attributes;

public class ColumnGroupFactory extends AbstractObjectCreationFactory {

    @Override
    public Object createObject(Attributes attributes) throws Exception {
        GlobalConfig globalConfig = (GlobalConfig) digester.peek(SchemaParser.GLOBAL_CONFIG);
        if (globalConfig == null) {
            throw new IllegalArgumentException("Global config is missing");
        }
        String name = attributes.getValue("name");
        ColumnGroupDef columnGroupDef = globalConfig.getColumnGroup(name);
        if (columnGroupDef == null) {
            throw new IllegalArgumentException("The column group is not defined");
        }
        return columnGroupDef;
    }

}
