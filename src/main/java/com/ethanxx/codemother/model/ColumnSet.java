package com.ethanxx.codemother.model;

import java.util.List;

/**
 * This is used to define a column collection used by some action.
 * @author Simon Xianyu
 */
public class ColumnSet {
	
	private String name;
	private String columns;
	
	private List<ColumnDef> columnList;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColumns() {
		return columns;
	}
	public void setColumns(String columns) {
		this.columns = columns;
	}
	public List<ColumnDef> getColumnList() {
		return columnList;
	}
	public void setColumnList(List<ColumnDef> columnList) {
		this.columnList = columnList;
	}

}
