package com.ethanxx.codemother.model;

public class ComplexKeyDef extends ColumnGroupDef implements KeyIntf {

    public ComplexKeyDef() {
    }

    @Override
    public boolean isSingle() {
        return false;
    }

    @Override
    public boolean isComplex() {
        return true;
    }

}
