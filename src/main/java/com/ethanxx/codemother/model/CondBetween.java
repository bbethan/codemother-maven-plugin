package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

public class CondBetween extends AbstractCondition {
	private String low;
	private String high;
	
	@Override
	public String getType() {
		return SearchTypeConst.BETWEEN;
	}
	
	public String getLow() {
		return low;
	}
	public void setLow(String low) {
		this.low = low;
	}
	public String getHigh() {
		return high;
	}
	public void setHigh(String high) {
		this.high = high;
	}
	
}
