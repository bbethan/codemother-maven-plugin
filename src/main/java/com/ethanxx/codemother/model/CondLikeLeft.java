package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

public class CondLikeLeft extends AbstractCondition {

	@Override
	public String getType() {
		return SearchTypeConst.LIKE_LEFT;
	}

}
