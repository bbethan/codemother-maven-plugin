package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

public class CondLikeRight extends AbstractCondition {

	@Override
	public String getType() {
		return SearchTypeConst.LIKE_RIGHT;
	}

}
