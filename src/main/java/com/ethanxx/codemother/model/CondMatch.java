package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

public class CondMatch extends AbstractCondition {
	@Override
	public String getType() {
		return SearchTypeConst.MATCH;
	}
}
