package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

public class CondMatchValue extends AbstractCondition {
	
	private String value;
	
	@Override
	public String getType() {
		return SearchTypeConst.MATCH_VALUE;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
