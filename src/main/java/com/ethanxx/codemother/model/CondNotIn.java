package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

/**
 * Created by ethan on 19/04/2017.
 */
public class CondNotIn extends AbstractCondition {
    @Override
    public String getType() {
        return SearchTypeConst.NOT_IN;
    }
}
