package com.ethanxx.codemother.model;

import com.ethanxx.codemother.utils.SearchTypeConst;

public class CondNotMatch extends AbstractCondition {
	@Override
	public String getType() {
		return SearchTypeConst.NOT_MATCH;
	}
}
