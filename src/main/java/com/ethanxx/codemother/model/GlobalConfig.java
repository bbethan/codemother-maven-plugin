package com.ethanxx.codemother.model;

import com.ethanxx.codemother.AbstractGlobalConfig;

import java.util.HashMap;
import java.util.Map;

public class GlobalConfig extends AbstractGlobalConfig {

    private Map<String, TypeDef> typeMap;
    private Map<String, ColumnGroupDef> columnGroupMap;

    public GlobalConfig() {
        typeMap = new HashMap<>();
        columnGroupMap = new HashMap<>();
    }

    public String toShortJavaType(ColumnDef column) {
        return toShortJavaType(column.getType());
    }

    public String toShortJavaType(String name) {
        TypeDef typeDef = typeMap.get(name);
        if (typeDef != null) {
            return typeDef.getShortJavaType();
        }
        return "{Type missing:" + name + "}";
    }

    public String toJdbcType(ColumnDef column) {
        TypeDef typeDef = typeMap.get(column.getType());
        if (typeDef != null) {
            return typeDef.getJdbcType();
        }
        return "{jdbcType missing for column:" + column.getName() + "}";
    }

    public String getJavaType(ColumnDef column) {
        TypeDef typeDef = typeMap.get(column.getType());
        if (typeDef != null) {
            return typeDef.getJavaType();
        }
        return "{Type missing for column:" + column.getName() + "}";
    }

    public TypeDef getTypeDef(String name) {
        return typeMap.get(name);
    }

    /**
     * @param typeDef
     * @date 2015年3月13日 下午10:46:42
     * @Descriptoin
     */
    public void addType(TypeDef typeDef) {
        if (typeMap.containsKey(typeDef.getName())) {
            throw new IllegalArgumentException("Type is duplicated:" + typeDef.getName());
        }
        typeMap.put(typeDef.getName(), typeDef);
    }

    public void addColumnGroup(ColumnGroupDef def) {
        columnGroupMap.put(def.getName(), def);
    }

    public ColumnGroupDef getColumnGroup(String name) {
        return columnGroupMap.get(name);
    }

}
