package com.ethanxx.codemother.model;

import static com.ethanxx.codemother.DigesterUtil.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

import com.ethanxx.codemother.AbstractGlobalConfig;
import com.ethanxx.codemother.GlobalConfigParserIntf;

public class GlobalConfigParser implements GlobalConfigParserIntf {

    private Digester createDig() {
        Digester dig = new Digester();
        createObjWithProps(dig, "global", GlobalConfig.class);
        addChildRule(dig, "global/types/type-def", TypeDef.class, "addType");
        addChildRule(dig, "global/column-group", ColumnGroupDef.class, "addColumnGroup");
        addChildRule(dig, "global/column-group/column", ColumnDef.class, "addColumn");

        return dig;
    }

    @Override
    public AbstractGlobalConfig parse(InputStream in) throws IOException, SAXException {
        Digester dig = createDig();
        return (AbstractGlobalConfig) dig.parse(in);
    }

}
