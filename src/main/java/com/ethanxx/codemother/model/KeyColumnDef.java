package com.ethanxx.codemother.model;

import org.codehaus.plexus.util.StringUtils;

public class KeyColumnDef extends ColumnDef implements KeyIntf {
    private String generator;

    public boolean isAuto() {
        return StringUtils.equals("native", generator);
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public boolean isComplex() {
        return false;
    }

    public String getGenerator() {
        return generator;
    }

    public void setGenerator(String generator) {
        this.generator = generator;
    }

}
