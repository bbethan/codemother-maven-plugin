package com.ethanxx.codemother.model;

public interface KeyIntf {

    boolean isSingle();

    boolean isComplex();
}
