package com.ethanxx.codemother.model;

public class OrderByDef {
    
    private String column;
    private boolean asc = true;
    
    public String getColumn() {
        return column;
    }
    public void setColumn(String column) {
        this.column = column;
    }
    public boolean isAsc() {
        return asc;
    }
    public void setAsc(boolean asc) {
        this.asc = asc;
    }

}
