package com.ethanxx.codemother.model;

public class RelatedAssoc extends AbstractRelated {
	private String column;
	@Override
	public String getTag() {
		return "association";
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
}
