package com.ethanxx.codemother.model;

import java.util.ArrayList;
import java.util.List;

public class ResultMapDef {
    private String name;
    private String alias;

    private List<AbstractRelated> relatedList = new ArrayList<AbstractRelated>();

    public void addRelated(AbstractRelated related) {
        relatedList.add(related);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AbstractRelated> getRelatedList() {
        return relatedList;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
