package com.ethanxx.codemother.model;

import com.ethanxx.codemother.AbstractSchemaDef;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class SchemaDef extends AbstractSchemaDef {

    private String schema;
    private String tablePrefix;
    private String tableName;
    private String modelName;
    private boolean grid;
    private String dispName;
    private boolean sortable;
    private boolean noController;
    private String urlname;

    private KeyIntf key;

    private Set<String> disabledMethodSet;
    private List<AbstractCondition> defaultConditionList = new ArrayList<>();
    private List<OrderByDef> defaultOrderByList = new ArrayList<>();
    private List<AliasDef> aliasList = new ArrayList<>();
    private List<ResultMapDef> resultMapList = new ArrayList<>();

    private List<ColumnDef> columnDefList;
    private List<ColumnDef> normalColumnDefList;
    private List<ColumnGroupDef> columnGroupList;
    private Map<String, ColumnDef> columnDefMap = new HashMap<>();
    private Map<String, ColumnSet> columnSetMap = new HashMap<>();


    public SchemaDef() {
        columnDefList = new ArrayList<>();
        normalColumnDefList = new ArrayList<>();
        columnGroupList = new ArrayList<>();
        disabledMethodSet = new HashSet<>();
    }

    @Override
    public void setGlobalProperties(Properties properties) {
        super.setGlobalProperties(properties);
        if (properties.containsKey("tablePrefix")) {
            this.tablePrefix = properties.getProperty("tablePrefix");
        }
    }

    public void addAlias(AliasDef alias) {
        aliasList.add(alias);
    }

    public void addCondition(AbstractCondition cond) {
        defaultConditionList.add(cond);
    }

    public void addResultMap(ResultMapDef def) {
        this.resultMapList.add(def);
    }

    public void addOrderBy(OrderByDef orderby) {
        defaultOrderByList.add(orderby);
    }

    public List<ColumnDef> getNormalColumnDefList() {
        return Collections.unmodifiableList(normalColumnDefList);
    }

    public List<ColumnDef> getColumnDefList() {
        return Collections.unmodifiableList(columnDefList);
    }

    /**
     * @param config
     * @return
     * @date 2015年3月16日 下午2:04:36
     * @Descriptoin 如果用到的类型不是基本类型，且不在 java.lang 包下，则需要 import
     */
    public Set<TypeDef> getImports(GlobalConfig config) {
        Set<TypeDef> types = new HashSet<>();
        for (ColumnDef column : columnDefList) {
            TypeDef type = config.getTypeDef(column.getType());
            if (type != null && !StringUtils.startsWith(type.getJavaType(), "java.lang.")
                    && !StringUtils.startsWith(type.getJavaType(), "java.sql.Timestamp")
                    && !StringUtils.startsWith(type.getJavaType(), "java.util.Date")
                    && isNotPrimary(type.getJavaType())) {
                types.add(type);
            }
        }
        return types;
    }

    private boolean isNotPrimary(String javaType) {
        return !("".equals(javaType) || "boolean".equals(javaType) || "byte".equals(javaType) || "char".equals(javaType)
                || "short".equals(javaType) || "int".equals(javaType) || "long".equals(javaType)
                || "float".equals(javaType) || "double".equals(javaType));
    }

    public boolean isAllowed(String method) {
        if (disabledMethodSet.contains(method)) {
            return false;
        }
        return true;
    }

    public void setDisabled(String methods) {
        String[] methodArr = StringUtils.split(methods, ",");
        for (String method : methodArr) {
            this.disabledMethodSet.add(method);
        }
    }

    public void setKey(KeyIntf key) {
        this.key = key;
        if (key instanceof ColumnDef) {
            ColumnDef column = (ColumnDef) key;
            columnDefList.add(column);
            columnDefMap.put(column.getName(), column);
        } else if (key instanceof ColumnGroupDef) {
            for (ColumnDef col : ((ColumnGroupDef) key).getColumnDefList()) {
                columnDefList.add(col);
            }
        }

    }

    public void addColumn(ColumnDef def) {
        columnDefList.add(def);
        normalColumnDefList.add(def);
        columnDefMap.put(def.getName(), def);
    }

    public void addColumnSet(ColumnSet columnSet) {
        String[] columnArray = StringUtils.split(columnSet.getColumns(), ",");
        List<ColumnDef> columnList = new ArrayList<>();
        for (String columnName : columnArray) {
            ColumnDef colDef = columnDefMap.get(StringUtils.trim(columnName));
            if (colDef == null) {
                throw new RuntimeException("Failed to find column " + columnName);
            }
            columnList.add(colDef);
        }
        columnSet.setColumnList(columnList);
        columnSetMap.put(columnSet.getName(), columnSet);
    }

    public ColumnSet findColumnSet(String name) {
        return columnSetMap.get(name);
    }

    public void addColumnGroup(ColumnGroupDef groupDef) {
        columnGroupList.add(groupDef);
        for (ColumnDef def : groupDef.getColumnDefList()) {
            columnDefList.add(def);
            normalColumnDefList.add(def);
            columnDefMap.put(def.getName(), def);
        }
    }

    protected String returnNotNull(String... strings) {
        for (String str : strings) {
            if (str != null) {
                return str;
            }
        }
        return null;
    }

    public String getModelName() {
        if (modelName == null) {
            modelName = StringUtils.capitalize(name);
        }
        return modelName;
    }

    public String getTableName() {
        if (tableName == null) {
            tableName = name;
        }
        if (StringUtils.isNotEmpty(tablePrefix)) {
            return tablePrefix + tableName;
        }
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public KeyIntf getKey() {
        return key;
    }

    public List<AbstractCondition> getDefaultConditionList() {
        return defaultConditionList;
    }

    public List<OrderByDef> getDefaultOrderByList() {
        return defaultOrderByList;
    }

    public List<AliasDef> getAliasList() {
        return aliasList;
    }

    public List<ResultMapDef> getResultMapList() {
        return resultMapList;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public String getDispName() {
        return dispName;
    }

    public void setDispName(String dispName) {
        this.dispName = dispName;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public boolean isGrid() {
        return grid;
    }

    public void setGrid(boolean grid) {
        this.grid = grid;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public boolean isNoController() {
        return noController;
    }

    public void setNoController(boolean noController) {
        this.noController = noController;
    }

    public String getUrlname() {
        return urlname;
    }

    public void setUrlname(String urlname) {
        this.urlname = urlname;
    }
}
