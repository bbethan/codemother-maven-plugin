package com.ethanxx.codemother.model;

import static com.ethanxx.codemother.DigesterUtil.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

import com.ethanxx.codemother.AbstractGlobalConfig;
import com.ethanxx.codemother.SchemaParserIntf;

public class SchemaParser implements SchemaParserIntf {

    public static final String GLOBAL_CONFIG = "global-config";
    private AbstractGlobalConfig globalConfig;

    public SchemaParser() {
    }

    private Digester createDigester() {
        Digester dig = new Digester();
        createObjWithProps(dig, "schema", SchemaDef.class);
        addChildRule(dig, "schema/key", KeyColumnDef.class, "setKey");
        addChildRule(dig, "schema/complex-key", ComplexKeyDef.class, "setKey");
        addChildRule(dig, "*/column", ColumnDef.class, "addColumn");

        addChildRule(dig, "schema/default-condition/between", CondBetween.class, "addCondition");
        addChildRule(dig, "schema/default-condition/in", CondIn.class, "addCondition");
        addChildRule(dig, "schema/default-condition/like", CondLike.class, "addCondition");
        addChildRule(dig, "schema/default-condition/like-left", CondLikeLeft.class, "addCondition");
        addChildRule(dig, "schema/default-condition/like-right", CondLikeRight.class, "addCondition");
        addChildRule(dig, "schema/default-condition/match", CondMatch.class, "addCondition");
        addChildRule(dig, "schema/default-condition/match-value", CondMatchValue.class, "addCondition");
        addChildRule(dig, "schema/default-condition/not-in", CondNotIn.class, "addCondition");
        addChildRule(dig, "schema/default-condition/not-match", CondNotMatch.class, "addCondition");

        addChildRule(dig, "schema/default-order/order-by", OrderByDef.class, "addOrderBy");

        addChildRule(dig, "schema/alias", AliasDef.class, "addAlias");
        addChildRule(dig, "schema/result-map", ResultMapDef.class, "addResultMap");
        addChildRule(dig, "schema/column-set", ColumnSet.class, "addColumnSet");
        addChildRule(dig, "*/collection", RelatedColl.class, "addRelated");
        addChildRule(dig, "*/association", RelatedAssoc.class, "addRelated");

        dig.addFactoryCreate("schema/column-group", ColumnGroupFactory.class);
        dig.addSetNext("schema/column-group", "addColumnGroup");

        dig.push(GLOBAL_CONFIG, globalConfig);
        return dig;
    }

    public SchemaDef parse(InputStream input) throws IOException, SAXException {
        SchemaDef def;
        Digester schemaDig = createDigester();
        def = (SchemaDef) schemaDig.parse(input);
        return def;
    }

    @Override
    public void init(AbstractGlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
        createDigester();
    }

    public AbstractGlobalConfig getGlobalConfig() {
        return globalConfig;
    }

    public void setGlobalConfig(AbstractGlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
    }
}
