package com.ethanxx.codemother.model;

public class TypeDef {

    private String name;
    private String javaType;
    private String jdbcType;
    private String dbType;
    private int defaultLength = -1;
    private int length;
    private boolean needLength;

    public String getShortJavaType() {
        if (javaType != null) {
            int lastIndexOf = javaType.lastIndexOf(".");
            if (lastIndexOf > 0) {
                return javaType.substring(lastIndexOf + 1);
            }
        }
        return javaType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public int getDefaultLength() {
        return defaultLength;
    }

    public void setDefaultLength(int defaultLength) {
        this.defaultLength = defaultLength;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isNeedLength() {
        return needLength;
    }

    public void setNeedLength(boolean needLength) {
        this.needLength = needLength;
    }
}
