package com.ethanxx.codemother.utils;

/**
 * Created by Ethan on 4/2.
 */
public class ConfigConstants {
    public static final String CONFIG = "./codemother/config.properties";
    public static final String SCHEMA_PATH = "./codemother/dbschema/";
    public static final String TEMPLATE_PATH = "./codemother/template";
    public static final String GLOBAL_PROPERTIES = "./src/main/resources/com/ethanxx/codemother/global.properties";
}
