package com.ethanxx.codemother.utils;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Created by ethan on 16/02/2017.
 */
public class LocalUtil {
    public static void readProperties(Properties properties, File propFile) throws IOException {
        if (properties == null) {
            throw new IllegalArgumentException("Properies should not be null");
        } else if (!propFile.exists()) {
            throw new IllegalArgumentException("Properties file doesn\'t exist :" + propFile.getName());
        } else {
            FileInputStream fin = null;

            try {
                fin = new FileInputStream(propFile);
                properties.load(fin);
            } finally {
                closeQuietly(fin);
            }

        }
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
            }
        }
    }

    public static String extractStringValue(Map<String, Object> map, String key) {
        return map.get(key) == null ? "" : map.get(key).toString();
    }

    public static String extractStringValue(Map<String, Object> map, String key, String defaultValue) {
        return map.get(key) == null ? defaultValue : map.get(key).toString();
    }

}
