package com.ethanxx.codemother.utils;

/**
 * Created by ethan on 16/02/2017.
 */
public class SearchTypeConst {
    public static final String MATCH = "match";
    public static final String NOT_MATCH = "not-match";
    public static final String MATCH_VALUE = "match-value";
    public static final String LIKE = "like";
    public static final String LIKE_LEFT = "like-left";
    public static final String LIKE_RIGHT = "like_right";
    public static final String BETWEEN = "between";
    public static final String IN = "in";
    public static final String NOT_IN = "not-in";
}
