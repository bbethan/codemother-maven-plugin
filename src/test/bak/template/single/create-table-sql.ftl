<#if schema.key.single && schema.key.generator?has_content && schema.key.generator = "native" >
DROP SEQUENCE seq_${schema.tableName}
/</#if>
DROP TABLE ${schema.tableName} cascade constraints
/
CREATE TABLE ${schema.tableName}
(
	<#if schema.key.complex>
	<#list schema.key.columnDefList as column>
	${column.name} ${config.getTypeDef(column.type).dbType}<#if column.length&gt;0 > (${column.length})</#if>,
	</#list>
	<#else>
	${schema.key.name} ${config.getTypeDef(schema.key.type).dbType} <#if schema.key.length&gt;0 >(${schema.key.length})</#if>,
	</#if>
	<#list schema.normalColumnDefList as column>
	${column.name} ${config.getTypeDef(column.type).dbType}<#if column.length&gt;0 > (${column.length})</#if>,
	</#list>
	primary key (<#if schema.key.complex ><#rt>
		<#list schema.key.columnDefList as column><#if column_index &gt; 0>,</#if>
			${column.name}<#rt>
		</#list><#rt>
	<#else> ${schema.key.name} <#rt>
	</#if><#lt>)
)
/
<#if schema.key.single && schema.key.generator?has_content && schema.key.generator = "native" >
CREATE SEQUENCE seq_${schema.tableName}
/
</#if>