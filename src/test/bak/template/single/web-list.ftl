<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%@include file="../taglib.jsp" %>
<#macro showVar name>${r"${"}${name}${r"}"}</#macro>
<#if schema.findColumnSet("table-columns")?has_content>
<sp:url var="gridUrl" value="/${schema.name}/aj_grid"></sp:url>
<sp:url var="addUrl" value="/${schema.name}/add"></sp:url>
<sp:url var="editUrl" value="/${schema.name}/edit"></sp:url>
<sp:url var="deleteUrl" value="/${schema.name}/aj_del_many"></sp:url>
<c:set var="grid_name" value="gridDiv" />
<c:set var="defaultSort" value="" />

<script type="text/javascript">
<!--
var grid = null;

pageInit = function() {
	// do init
	$.ligerDefaults.Grid.root="rows";
	$.ligerDefaults.Grid.record="total";
	grid = $("#<@showVar 'grid_name'/>").ligerGrid({
		url: "<@showVar 'gridUrl'/>",dataAction:'server',
		columns: [
		<#list schema.findColumnSet("table-columns").columnList as column>
		  { display: "${column.dispName!}", name: "${column.javaName}" <#rt />
		<#lt /><#if column.type='boolean'>,render: function(row) { return row.enabled === true?'是':'否'; }</#if><#rt />
		<#lt /><#if column.type='date' >,render: function (row) {
        	  return Yun.formatDate(new Date(row.lastLogin), 'yyyy-MM-dd h:s');
          	}</#if><#rt />
        <#lt />},
		</#list>
          { 
        	  display: '操作', isAllowHide: false, 
        	  render: function (row) 
        	  {
	        	  var html = '<a target="tab" tabid="edit_${schema.name}" title="编辑${schema.dispName}" 
	        	   href="<@showVar 'editUrl'/>?${schema.key.name}=' + row.${schema.key.javaName} + '" ><span>编辑</span></a>'; 
	        	  return html;
        	  } 
       	  }
          ], pageSize: 30, 
          checkbox : true,
          width: '100%',height:'100%'
          ,onAfterShowData: function () {
        	  Yun.resolveTargets("#<@showVar 'grid_name'/>");
          }
          
	});
	
	Yun.data['tab'] = window.parent.tab;
}
//-->
</script>

<div id="filter_bar">
<form method="post" onsubmit="doUpdate(); return false;">
<#list schema.columnDefList as column><#if column.searchType?has_content && column.searchType!='none'>
${column.dispName}:
<#switch column.type>
<#default>
<input type="text" name="q_${column.name}" id="q_${column.name}" >
</#switch>
</#if>
</#list>
<input type="button" onclick="doUpdate()" value="过滤" >
</form>
</div>

<sec:authorize url="/${schema.name}/edit">

<div style="height:28px; display:block;">
<a class="hbtn" href="<@showVar 'addUrl'/>" target="tab" tabid="edit_${schema.name}" title="编辑${schema.dispName}"><span>新建</span></a>
<a class="hbtn" href="<@showVar 'deleteUrl'/>" target="deleteGrid" msg="确认删除选中的${schema.dispName}吗？" grid="<@showVar 'grid_name'/>" 
idname="username"><span>删除</span></a>
</div>
<div id="gridDiv">
</div>

</sec:authorize>

<div id="<@showVar 'dt_name' />"></div> 
</#if>