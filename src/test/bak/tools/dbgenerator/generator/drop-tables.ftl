<#list schemaList as schema><#t />
DROP TABLE ${schema.tableName} cascade constraints
/
</#list>

<#list schemaList as schema><#t />
<#if !schema.key.complex>DROP SEQUENCE seq_${schema.tableName}
/</#if>
</#list>