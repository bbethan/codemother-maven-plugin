<#macro listKey>
	<#if schema.key.complex>
		<#list schema.key.columnDefList as column>
			${column.name} = ${r"#{"}${column.javaName}${r"}"}<#if column_has_next> AND </#if><#t />
		</#list>
	<#else>
		${schema.key.name} = ${r"#{"}${schema.key.javaName}${r"}"}<#t />
	</#if>
</#macro><#t />