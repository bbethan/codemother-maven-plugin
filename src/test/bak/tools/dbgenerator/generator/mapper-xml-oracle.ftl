<#t /><#include "mapper-xml-inc.ftl" />
<#macro insert>
		<#if !schema.key.complex && schema.key.generator?has_content && schema.key.generator="sequence">
		<selectKey keyProperty="${schema.key.javaName}" resultType="${config.toJavaType(schema.key)}" order="BEFORE">
		   SELECT seq_${schema.tableName}.nextval FROM DUAL
		</selectKey>
		</#if>
		INSERT INTO ${schema.tableName} (
		  <#list schema.columnDefList as columnDef >
		  	${columnDef.name} <#if columnDef_has_next> ,</#if>
		  </#list>
		) VALUES(
		  <#list schema.columnDefList as columnDef >
		  	<#rt /><#if columnDef.insertValue?has_content>${columnDef.insertValue}<#rt />
		  	<#else>${r"#{"}${columnDef.javaName},jdbcType=${config.toJdbcType(columnDef)}${r"}"}<#rt />
		  	<#lt /></#if><#if columnDef_has_next> ,</#if>
		  </#list>
		)
</#macro>
<#macro insertOrUpdate>
	MERGE INTO ${schema.tableName}
		USING (SELECT count(*) as co FROM ${schema.tableName} 
			WHERE <@listKey />) t
		ON (t.co &lt;&gt; 0)
		WHEN NOT MATCHED THEN
			INSERT (
			  <#list schema.columnDefList as columnDef >
			  	${columnDef.name} <#if columnDef_has_next> ,</#if>
			  </#list>
			) VALUES(
			  <#list schema.columnDefList as columnDef >
			  	<#rt /><#if columnDef.insertValue?has_content>${columnDef.insertValue}<#rt />
			  	<#else>${r"#{"}${columnDef.javaName},jdbcType=${config.toJdbcType(columnDef)}${r"}"}<#rt />
			  	<#lt /></#if><#if columnDef_has_next> ,</#if>
			  </#list>
			)
		WHEN MATCHED THEN
			UPDATE SET
		      <#list schema.normalColumnDefList as columnDef >
		        <#if !columnDef.noUpdate>
			  	${columnDef.name} = <#rt />
			  	<#lt /><#if columnDef.updateValue?has_content>${columnDef.updateValue}<#rt />
			  	<#lt /><#else>${r"#{"}${columnDef.javaName},jdbcType=${config.toJdbcType(columnDef)}${r"}"}<#rt />
			  	<#lt /></#if><#if columnDef_has_next>,</#if>
			  	</#if>
			  </#list>
			WHERE <@listKey />
</#macro>