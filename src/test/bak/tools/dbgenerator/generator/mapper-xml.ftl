<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${mapperPackage}.${schema.modelName}Mapper">
<#macro selectColList>
		<#list schema.columnDefList as columnDef >
		  ${columnDef.name} as ${columnDef.javaName} <#if columnDef_has_next> ,</#if>
		</#list>
</#macro><#t />
<#t /><#macro defaultWhere withOrderBy=false>
		<where>
		  <#if schema.defaultConditionSet?has_content ><#list schema.defaultConditionSet.conditionList as searchCond>
		    <#if searchCond.type="fixed">
		    <#if searchCond_index gt 0>AND</#if> ${searchCond.column} = ${searchCond.value}
		    <#elseif searchCond.type="match">
		    <if test="${searchCond.column} !=null">
		    	AND ${searchCond.column} = ${r"#{"}${searchCond.column}${r"}"}
		    </if>
		    <#elseif searchCond.type="between">
		    <if test="${searchCond.paramStart} !=null AND ${searchCond.paramEnd} !=null">
		    	AND ${searchCond.column} between ${r"#{"}${searchCond.paramStart}${r"}"} and ${r"#{"}${searchCond.paramEnd}${r"}"}
		    </if>
		    </#if>
		  </#list></#if><#t />
	    </where>
	    <#if withOrderBy && schema.defaultConditionSet?has_content>
	    	<@listOrderBy schema.defaultConditionSet.orderByList/>
	    </#if>	  
</#macro><#t />
<#t /><#macro listOrderBy orderByList>
	<#if orderByList!?size gt 0>
		<#list orderByList as orderBy>
		<#if orderBy_index==0>ORDER BY </#if><#rt />
		  	<#lt />${orderBy.column}<#if !orderBy.asc> DESC</#if><#if orderBy_has_next>,</#if><#rt />
		</#list>
	</#if>
</#macro>
<#t /><#macro searchCondition>
		  <#list schema.searchConditionList as searchCond>
		    <#if searchCond.type="fixed">
		    <#if searchCond_index gt 0>AND</#if> ${searchCond.column} = ${searchCond.value}
		    <#elseif searchCond.type="match">
		    <if test="${searchCond.column} !=null">
		    	AND ${searchCond.column} = ${r"#{"}${searchCond.column}${r"}"}
		    </if>
		    <#elseif searchCond.type="between">
		    <if test="${searchCond.paramStart} !=null AND ${searchCond.paramEnd} !=null">
		    	AND ${searchCond.column} between ${r"#{"}${searchCond.paramStart}${r"}"} and ${r"#{"}${searchCond.paramEnd}${r"}"}
		    </if>
		    </#if>
		  </#list>
</#macro><#t />
<#t /><#include "mapper-xml-inc.ftl" />
<#macro listKey>
	<#if schema.key.complex>
		<#list schema.key.columnDefList as column>
			${column.name} = ${r"#{"}${column.javaName}${r"}"}<#if column_has_next> AND </#if><#t />
		</#list>
	<#else>
		${schema.key.name} = ${r"#{"}${schema.key.javaName}${r"}"}<#t />
	</#if>
</#macro><#t />
<#t /><#import "mapper-xml-${config.databaseType}.ftl" as dblib />

	<!-- auto generate start -->
	<select id="countAll" resultType="java.lang.Integer">
		SELECT COUNT(*) as total FROM ${schema.tableName}
	</select>
	
	<select id="countBy" resultType="java.util.Map">
		SELECT COUNT(*) as total FROM ${schema.tableName}
		<@defaultWhere false />
	</select>
	
	<select id="listAll" resultType="${schema.modelName}" >
		SELECT
		  <@selectColList />
		FROM ${schema.tableName}
		<#if schema.defaultOrderBySet?has_content>
		<@listOrderBy schema.defaultOrderBySet.orderByDefList />
		</#if>
	</select>
	
	<select id="listAllPaged" resultType="${schema.modelName}" >
	<#if schema.paged>
	SELECT * FROM (
	SELECT t.*, rownum as rn FROM (
	</#if>
		SELECT
		  <@selectColList />
		FROM ${schema.tableName}
		<#if schema.defaultOrderBySet?has_content>
		<@listOrderBy schema.defaultOrderBySet.orderByDefList />
		</#if>
	<#if schema.paged>
	) t WHERE rownum &lt;= ${r"#{"}end${r"}"}
	) WHERE rn &gt;=${r"#{"}begin${r"}"}
	</#if>
	</select>
	
	<select id="listBy" resultType="${schema.modelName}" >
	<#if schema.paged>
	SELECT * FROM (
	SELECT t.*, rownum as rn FROM (
	</#if>
		SELECT
		  <#list schema.columnDefList as columnDef >
		  	${columnDef.name} as ${columnDef.javaName} <#if columnDef_has_next> ,</#if>
		  </#list>
		FROM ${schema.tableName}
		<@defaultWhere true/>
	<#if schema.paged>
	) t WHERE rownum &lt;= ${r"#{"}end${r"}"}
	) WHERE rn &gt;=${r"#{"}begin${r"}"}
	</#if>
	</select>
	
	<insert id="insert" parameterType="${schema.modelName}">
		<@dblib.insert />
	</insert>
	
	<update id="update" parameterType="${schema.modelName}">
		UPDATE ${schema.tableName} SET <#assign columnBegin=true />
	      <#list schema.normalColumnDefList as columnDef >
	        <#lt /><#if !columnDef.noUpdate>
	        <#lt /><#if columnBegin><#assign columnBegin=false /><#else>,</#if>
		  	${columnDef.name} = <#rt />
		  	<#lt /><#if columnDef.updateValue?has_content>${columnDef.updateValue}<#rt />
		  	<#lt /><#else>${r"#{"}${columnDef.javaName},jdbcType=${config.toJdbcType(columnDef)}${r"}"}<#rt />
		  	<#lt /></#if>
		  	</#if>
		  </#list>
		WHERE <@listKey />
	</update>
	
	<#if  schema.key.complex || !(schema.key.generator?has_content)>
	<update id="insertOrUpdate" parameterType="${schema.modelName}">
		<@dblib.insertOrUpdate />
	</update>
	</#if>
	
	<delete id="deleteByKey" <#if !schema.key.complex>parameterType="${config.toJavaType(schema.key)}"</#if>>
		DELETE FROM ${schema.tableName}
		WHERE <@listKey />
	</delete>
	
	<delete id="deleteByModelKey" parameterType="${schema.modelName}">
		DELETE FROM ${schema.tableName}
		WHERE <@listKey />
	</delete>
	
	<select id="getByKey" <#if !schema.key.complex>parameterType="${config.toJavaType(schema.key)}"</#if> resultType="${schema.modelName}">
		SELECT
		  <@selectColList />
		FROM ${schema.tableName}
		WHERE <@listKey />
	</select>
	
	<sql id="selectColumns">
		<@selectColList />
	</sql>
	
	<#list schema.aliases as alias>
	<sql id="alias_${alias}">
		<#list schema.columnDefList as columnDef >
		  ${alias}.${columnDef.name} as ${alias}_${columnDef.name} <#if columnDef_has_next> ,</#if>
		</#list>
	</sql>
	</#list>
	
	<#list schema.aliasColumns as alias>
	<sql id="alias_col_${alias}">
		<#list schema.columnDefList as columnDef >
		  ${alias}.${columnDef.name} as ${columnDef.javaName} <#if columnDef_has_next> ,</#if>
		</#list>
	</sql>
	</#list>
	
	<#macro renderRelated node>
	<#if node.relatedList?exists>
	<#list node.relatedList as related>
		<#assign subSchema= config.getSchema(related.schemaName) />
		<#assign tagName = related.tag />
		<#if related.tag == 'collection'>
		<collection property="${related.property}" ofType="${subSchema.modelName}">
		</#if>
		<#if related.tag == 'association'>
		<association property="${related.property}" column="${related.column}" javaType="${subSchema.modelName}">
		</#if>
		<#if subSchema.key.complex>
		<#list subSchema.key.columnDefList as columnDef >
			<id property="${columnDef.javaName}" column="${related.alias}_${columnDef.name}"/>
	    </#list>
		<#else>
			<id property="${subSchema.key.javaName}" column="${related.alias}_${subSchema.key.name}"/>
		</#if>
		<#list subSchema.normalColumnDefList as columnDef >
		<result property="${columnDef.javaName}" column="${related.alias}_${columnDef.name}"/>
	    </#list>
	    <@renderRelated related/>
    	</${tagName}>
    </#list>
    </#if>
	</#macro>
	
	<#macro renderColumns relatedColl>
		<#assign subSchema= config.getSchema(relatedColl.schemaName) />
		<#list subSchema.columnDefList as columnDef >
		,${relatedColl.alias}.${columnDef.name} as ${relatedColl.alias}_${columnDef.name}
	    </#list>
	    <#list relatedColl.relatedList as subCollection>
	    	<@renderColumns subCollection />
	    </#list>
	</#macro>
	
	<#list schema.resultMapList as resultMap>
	<resultMap type="${schema.modelName}" id="${resultMap.name}">
		<#if schema.key.complex>
		<#else>
		<id property="${schema.key.javaName}" column="${schema.key.name}"/>
		</#if>
		<#list schema.normalColumnDefList as columnDef >
		<result property="${columnDef.javaName}" column="${columnDef.name}"/>
	    </#list>
    	<@renderRelated resultMap />
	</resultMap>
	<sql id="${resultMap.name}_columns">
		<#list schema.columnDefList as columnDef >
		<#if columnDef_index gt 0>,</#if> ${resultMap.alias}.${columnDef.name} as ${columnDef.name}
		</#list>
	    <#list resultMap.relatedList as relatedColl>
    	<@renderColumns relatedColl />
	    </#list>
	</sql>
	</#list>
	<!-- auto generate end -->
	
	<!-- manual xml blocks start -->
<#if manualContent?has_content>${manualContent}</#if><#t />
	<!-- manual xml blocks end -->
	
</mapper>
