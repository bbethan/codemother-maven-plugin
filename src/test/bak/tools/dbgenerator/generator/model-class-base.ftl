package ${modelPackage}.base;

<#list imports as importLine>
import ${importLine.javaType};
</#list>

public class Base${schema.modelName}
{
	<#list schema.columnDefList as columnDef>
	protected ${config.toShortJavaType(columnDef)} ${columnDef.javaName};
	</#list>
	
	<#list schema.columnDefList as columnDef>
	public ${config.toShortJavaType(columnDef)} get${columnDef.javaName?cap_first}() {
		return ${columnDef.javaName};
	}
	public void set${columnDef.javaName?cap_first} (${config.toShortJavaType(columnDef)} val) {
		this.${columnDef.javaName} = val;
	}
	
	</#list>
}