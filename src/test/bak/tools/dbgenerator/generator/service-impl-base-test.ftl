package ${servicePackage}.impl.base;

import java.util.List;
import java.util.Map;

import ${modelPackage}.${schema.modelName};
import ${mapperPackage}.${schema.modelName}Mapper;
import ${supportPackage}.*;
import ${servicePackage}.base.Base${schema.modelName}Service;

public class Base${schema.modelName}ServiceImpl
	extends ${baseServiceImpl}
	implements Base${schema.modelName}Service
{
	
	public Integer countAll() {
		return this.myBatis3Template.query(${schema.modelName}Mapper.class,new MapperCallback<${schema.modelName}Mapper,Integer>(){
			public Integer execute(${schema.modelName}Mapper mapper) {
				return mapper.countAll();
			}
		});
	}
	
	public Integer countBy(final Map<String,Object> params) {
		return this.myBatis3Template.query(${schema.modelName}Mapper.class,new MapperCallback<${schema.modelName}Mapper,Integer>(){
			public Integer execute(${schema.modelName}Mapper mapper) {
				return mapper.countBy(params);
			}
		});
	}
	
	public List<${schema.modelName}> listAll() {
		return this.myBatis3Template.query(${schema.modelName}Mapper.class,new MapperCallback<${schema.modelName}Mapper,Integer>(){
			public List<${schema.modelName}> execute(${schema.modelName}Mapper mapper) {
				return mapper.listAll();
			}
		});
	}

	public List<${schema.modelName}> list(Map<String,Object> params){
		return null;
	}
	
	public Object insert(${schema.modelName} model) {
		return null;
	}
	
	public Object update(${schema.modelName} model) {
		return null;
	}

	public void deleteByKey(Long key) {
	}
	
	public void deleteByModel(${schema.modelName} model) {
	}
	
	public ${schema.modelName} getByKey(${config.toShortJavaType(schema.key)} key) {
		return null;
	}
}