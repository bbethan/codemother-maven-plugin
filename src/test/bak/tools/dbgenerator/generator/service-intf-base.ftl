package ${servicePackage}.base;

import java.util.List;
import java.util.Map;

import ${modelPackage}.${schema.modelName};

<#macro listKeyParam>
<#if schema.key.complex>
<#list schema.key.columnDefList as columnDef>
${config.toShortJavaType(columnDef)} ${columnDef.javaName}<#if columnDef_has_next>,</#if><#rt />
</#list>
<#else>
${config.toShortJavaType(schema.key)} key<#rt />
</#if><#rt />
</#macro><#t />

public interface Base${schema.modelName}Service
{
<#if schema.isAllowed("countAll")>
	Integer countAll();
</#if>
<#if schema.isAllowed("schema.isAllowed")>	
	Integer countBy(Map<String,Object> params);
</#if>
<#if schema.isAllowed("listAll")>	
	List<${schema.modelName}> listAll();
<#if schema.paged>
	List<${schema.modelName}> listAllPaged(long begin, long end);
</#if>
</#if>
<#if schema.isAllowed("listBy")>
	List<${schema.modelName}> listBy(Map<String,Object> params);
</#if>
<#if schema.isAllowed("insert")>	
	Object insert(${schema.modelName} model);
</#if>
<#if schema.isAllowed("update")>	
	void update(${schema.modelName} model);
</#if>
<#if schema.isAllowed("deleteByKey")>
	void deleteByKey(<@listKeyParam />);
</#if>
<#if schema.isAllowed("deleteByModel")>	
	void deleteByModel(${schema.modelName} model);
</#if>
<#if schema.isAllowed("getByKey")>
	${schema.modelName} getByKey(<@listKeyParam />);
</#if>
}