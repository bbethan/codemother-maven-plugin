<#macro listKey>
	<#if schema.key.complex>
		<#list schema.key.columnDefList as column><#t />
			${column.name} = ""<#if column_has_next> AND </#if><#t />
		</#list><#t />
	<#else>
		${schema.key.name} = ""<#t />
	</#if>
</#macro><#t />

INSERT INTO ${schema.tableName} (
<#t /><#list schema.columnDefList as column><#rt />
<#lt />${column.name}<#if column_has_next >, </#if><#rt />
<#lt /></#list><#rt />
<#lt />) VALUES (<#lt /><#list schema.columnDefList as column>
"${column.name}"<#if column_has_next >, </#if><#t />
</#list>)
/

UPDATE ${schema.tableName}
SET <#lt /><#list schema.columnDefList as column>
${column.name}=""<#if column_has_next >, </#if><#t />
</#list>

WHERE <@listKey />
/

DELETE FROM ${schema.tableName}
WHERE <@listKey />
/