package com.ethanxx.codemother;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Test;

public class MotherMojoTest {
	private MotherMojo mojo;
	
	@Before
	public void setup() {
		mojo = new MotherMojo();
		MavenProject project = new MavenProject();
		project.setFile(new File("."));
		mojo.setProject(project);
        // 目录下内容不变
        mojo.setCodemotherPath("/com/ethanxx/codemother");
        mojo.setConfigPath("/com/ethanxx/codemother/dbschema/config");
        mojo.setTemplatePath("./codemother/template");
        // 目录下内容根据项目设置
        mojo.setGolbalConfigPath("src/main/resources/com/ethanxx/codemother");
		mojo.setSchemaPath("src/main/resources/com/ethanxx/codemother/dbschema");

		mojo.setOutputLog("testOut/outfile.lst");
		mojo.setGlobalConfigParserClassName("com.ethanxx.codemother.model.GlobalConfigParser");
		mojo.setSchemaParserClassName("com.ethanxx.codemother.model.SchemaParser");
	}

	@Test
	public void testExecute() throws MojoExecutionException, MojoFailureException {
		mojo.execute();
	}

}
