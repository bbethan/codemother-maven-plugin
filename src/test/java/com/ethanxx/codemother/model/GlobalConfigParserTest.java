package com.ethanxx.codemother.model;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.ethanxx.codemother.AbstractGlobalConfig;
import com.ethanxx.codemother.model.GlobalConfigParser;

public class GlobalConfigParserTest {
	private GlobalConfigParser parser;

	@Before
	public void setUp() throws Exception {
		parser = new GlobalConfigParser();
	}

	@Test
	public void testParse() {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream("/com/ethanxx/codemother/dbschema/config/global.xml");
			AbstractGlobalConfig global = parser.parse(in);
			assertNotNull("Failed to parse global config", global);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Failed to read test global.xml");
		} catch (SAXException e) {
			e.printStackTrace();
			fail("Failed to parse xml");
		} finally {
			IOUtils.closeQuietly(in);
		}
		
	}

}
