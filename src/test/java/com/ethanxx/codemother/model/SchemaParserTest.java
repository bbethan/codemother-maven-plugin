package com.ethanxx.codemother.model;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class SchemaParserTest {

	private SchemaParser parser ;
	@Before
	public void setUp() throws Exception {
		parser = new SchemaParser();
		GlobalConfig globalConfig = new GlobalConfig();
		ColumnGroupDef groupDef = new ColumnGroupDef();
		groupDef.setName("trackModify");
		globalConfig.addColumnGroup(groupDef);
		parser.init(globalConfig);
	}

	@Test
	public void testParse() {
		InputStream in;
		try {
			in = this.getClass().getResourceAsStream("/com/ethanxx/codemother/dbschema/BaseUser.xml");
			SchemaDef def = parser.parse(in);
			assertNotNull("No result of schema", def);
		} catch (IOException e) {
			fail("Failed in read from file");
		} catch (SAXException e) {
			fail("Failed to parse xml");
		}
		
	}

}
