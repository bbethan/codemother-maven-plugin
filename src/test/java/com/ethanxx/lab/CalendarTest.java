package com.ethanxx.lab;

import org.junit.Test;

import java.util.Calendar;

/**
 * Created by ethan on 04/04/2017.
 */
public class CalendarTest {

    @Test
    public void getActualMaximumTest() {
        Calendar c1 = Calendar.getInstance();
//        c1.set(2017, 3, 1); // 2017-4-1
        c1.set(2017, 1, 1); // 2017-2-1
        System.out.print("When c1 is: ");
        System.out.println(c1.getTime());

        Calendar c2 = Calendar.getInstance();
        c2.set(2017, 4, 1); // 2017-5-1
//        c2.set(2017, 6, 1); // 2017-7-1
//        c2.set(2017, 3, 1); // 2017-4-1
        System.out.print("And c2 is: ");
        System.out.println(c2.getTime());

//        c2.set(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c2.getActualMaximum(Calendar.DAY_OF_MONTH));
        c2.set(Calendar.YEAR, c1.get(Calendar.YEAR));
        c2.set(Calendar.MONTH, c1.get(Calendar.MONTH));
        c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
        System.out.print("Than the result is: ");
        System.out.println(c2.getTime());
    }
}
